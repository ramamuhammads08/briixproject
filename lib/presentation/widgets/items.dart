import 'package:brix_test/data/models/item_genre_model.dart';
import 'package:brix_test/data/models/movie_model.dart';
import 'package:flutter/material.dart';

class MovieItem extends StatelessWidget {
  final MovieModel? movie;
  final Function(MovieModel)? onTap;
  const MovieItem({this.movie, this.onTap, super.key});

  @override
  Widget build(BuildContext context) {
    List<ItemGenreModel> filterGenreModel =
        movie!.genres!.where((element) => element.isSelected).toList();
    return InkWell(
      borderRadius: BorderRadius.circular(16),
      onTap: () {
        onTap!(movie!);
      },
      child: Container(
        width: MediaQuery.sizeOf(context).width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            border: Border.all(color: Colors.black87, width: 0.5)),
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(movie?.title ?? '',
                  style: const TextStyle(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 16)),
              Text(movie?.director ?? '', style: const TextStyle(fontSize: 16)),
              const SizedBox(height: 20),
              Align(
                alignment: Alignment.centerRight,
                child: SizedBox(
                  height: 20,
                  child: ListView.separated(
                      physics: const NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemBuilder: (context, index) => Text(
                          filterGenreModel[index].label,
                          style: const TextStyle(fontSize: 16)),
                      separatorBuilder: (context, index) => const Text(' | '),
                      itemCount: filterGenreModel.length),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
