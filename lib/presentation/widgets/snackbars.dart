import 'package:flutter/material.dart';

showAlertSnackBar(BuildContext context, String message) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content:
          Text(message, style: const TextStyle(fontWeight: FontWeight.bold)),
      backgroundColor: Colors.redAccent,
      duration: const Duration(milliseconds: 1500)));
}

showSuccessSnackBar(BuildContext context, String message) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content:
          Text(message, style: const TextStyle(fontWeight: FontWeight.bold)),
      backgroundColor: Colors.green,
      duration: const Duration(milliseconds: 1500)));
}
