import 'package:flutter/material.dart';

class SearchTextField extends StatelessWidget {
  final Function(String)? onChanged;
  const SearchTextField({this.onChanged, super.key});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChanged,
      cursorColor: Colors.black87,
      decoration: const InputDecoration(
          prefixIcon: Icon(Icons.search, color: Colors.black87),
          contentPadding: EdgeInsets.symmetric(horizontal: 16),
          focusedBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.black87)),
          border:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.black87)),
          hintText: 'Search'),
    );
  }
}

class CustomTextFormField extends StatelessWidget {
  final TextEditingController? controller;
  final String? label;
  final int? maxLines;
  final Function(String)? onChanged;
  const CustomTextFormField(
      {this.controller, this.label, this.maxLines, this.onChanged, super.key});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      onChanged: onChanged,
      cursorColor: Colors.black87,
      maxLines: maxLines ?? 1,
      keyboardType: TextInputType.multiline,
      decoration: InputDecoration(
          floatingLabelStyle: const TextStyle(color: Colors.black87),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black87)),
          border: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black87)),
          label: Text(label ?? '')),
    );
  }
}
