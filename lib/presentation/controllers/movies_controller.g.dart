// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movies_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MoviesController on _MoviesController, Store {
  late final _$listMoviesAtom =
      Atom(name: '_MoviesController.listMovies', context: context);

  @override
  ObservableList<MovieModel> get listMovies {
    _$listMoviesAtom.reportRead();
    return super.listMovies;
  }

  @override
  set listMovies(ObservableList<MovieModel> value) {
    _$listMoviesAtom.reportWrite(value, super.listMovies, () {
      super.listMovies = value;
    });
  }

  late final _$listFilterMoviesAtom =
      Atom(name: '_MoviesController.listFilterMovies', context: context);

  @override
  ObservableList<MovieModel> get listFilterMovies {
    _$listFilterMoviesAtom.reportRead();
    return super.listFilterMovies;
  }

  @override
  set listFilterMovies(ObservableList<MovieModel> value) {
    _$listFilterMoviesAtom.reportWrite(value, super.listFilterMovies, () {
      super.listFilterMovies = value;
    });
  }

  late final _$fetchMoviesAsyncAction =
      AsyncAction('_MoviesController.fetchMovies', context: context);

  @override
  Future<dynamic> fetchMovies() {
    return _$fetchMoviesAsyncAction.run(() => super.fetchMovies());
  }

  late final _$_MoviesControllerActionController =
      ActionController(name: '_MoviesController', context: context);

  @override
  void searchMovies(String keyword) {
    final _$actionInfo = _$_MoviesControllerActionController.startAction(
        name: '_MoviesController.searchMovies');
    try {
      return super.searchMovies(keyword);
    } finally {
      _$_MoviesControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
listMovies: ${listMovies},
listFilterMovies: ${listFilterMovies}
    ''';
  }
}
