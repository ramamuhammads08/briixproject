// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_movie_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$DetailMovieController on _DetailMovieController, Store {
  late final _$titleMovieAtom =
      Atom(name: '_DetailMovieController.titleMovie', context: context);

  @override
  String? get titleMovie {
    _$titleMovieAtom.reportRead();
    return super.titleMovie;
  }

  @override
  set titleMovie(String? value) {
    _$titleMovieAtom.reportWrite(value, super.titleMovie, () {
      super.titleMovie = value;
    });
  }

  late final _$directorMovieAtom =
      Atom(name: '_DetailMovieController.directorMovie', context: context);

  @override
  String? get directorMovie {
    _$directorMovieAtom.reportRead();
    return super.directorMovie;
  }

  @override
  set directorMovie(String? value) {
    _$directorMovieAtom.reportWrite(value, super.directorMovie, () {
      super.directorMovie = value;
    });
  }

  late final _$summaryMovieAtom =
      Atom(name: '_DetailMovieController.summaryMovie', context: context);

  @override
  String? get summaryMovie {
    _$summaryMovieAtom.reportRead();
    return super.summaryMovie;
  }

  @override
  set summaryMovie(String? value) {
    _$summaryMovieAtom.reportWrite(value, super.summaryMovie, () {
      super.summaryMovie = value;
    });
  }

  late final _$listGenresMovieAtom =
      Atom(name: '_DetailMovieController.listGenresMovie', context: context);

  @override
  ObservableList<ItemGenreModel> get listGenresMovie {
    _$listGenresMovieAtom.reportRead();
    return super.listGenresMovie;
  }

  @override
  set listGenresMovie(ObservableList<ItemGenreModel> value) {
    _$listGenresMovieAtom.reportWrite(value, super.listGenresMovie, () {
      super.listGenresMovie = value;
    });
  }

  late final _$addOrUpdateNewMovieAsyncAction = AsyncAction(
      '_DetailMovieController.addOrUpdateNewMovie',
      context: context);

  @override
  Future<dynamic> addOrUpdateNewMovie(BuildContext context,
      {MovieModel? movie}) {
    return _$addOrUpdateNewMovieAsyncAction
        .run(() => super.addOrUpdateNewMovie(context, movie: movie));
  }

  late final _$deleteMovieAsyncAction =
      AsyncAction('_DetailMovieController.deleteMovie', context: context);

  @override
  Future deleteMovie(BuildContext context, MovieModel movie) {
    return _$deleteMovieAsyncAction
        .run(() => super.deleteMovie(context, movie));
  }

  late final _$_DetailMovieControllerActionController =
      ActionController(name: '_DetailMovieController', context: context);

  @override
  void setTitle(String value) {
    final _$actionInfo = _$_DetailMovieControllerActionController.startAction(
        name: '_DetailMovieController.setTitle');
    try {
      return super.setTitle(value);
    } finally {
      _$_DetailMovieControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setDirector(String value) {
    final _$actionInfo = _$_DetailMovieControllerActionController.startAction(
        name: '_DetailMovieController.setDirector');
    try {
      return super.setDirector(value);
    } finally {
      _$_DetailMovieControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setSummary(String value) {
    final _$actionInfo = _$_DetailMovieControllerActionController.startAction(
        name: '_DetailMovieController.setSummary');
    try {
      return super.setSummary(value);
    } finally {
      _$_DetailMovieControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void selectGenres(int index) {
    final _$actionInfo = _$_DetailMovieControllerActionController.startAction(
        name: '_DetailMovieController.selectGenres');
    try {
      return super.selectGenres(index);
    } finally {
      _$_DetailMovieControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fetchGenres({List<ItemGenreModel>? listGenres}) {
    final _$actionInfo = _$_DetailMovieControllerActionController.startAction(
        name: '_DetailMovieController.fetchGenres');
    try {
      return super.fetchGenres(listGenres: listGenres);
    } finally {
      _$_DetailMovieControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic disposeData() {
    final _$actionInfo = _$_DetailMovieControllerActionController.startAction(
        name: '_DetailMovieController.disposeData');
    try {
      return super.disposeData();
    } finally {
      _$_DetailMovieControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
titleMovie: ${titleMovie},
directorMovie: ${directorMovie},
summaryMovie: ${summaryMovie},
listGenresMovie: ${listGenresMovie}
    ''';
  }
}
