// ignore_for_file: use_build_context_synchronously
import 'package:auto_route/auto_route.dart';
import 'package:brix_test/app/routers/app_router.gr.dart';
import 'package:brix_test/core/di/locator.dart';
import 'package:brix_test/core/utils/extensions.dart';
import 'package:brix_test/core/utils/util.dart';
import 'package:brix_test/data/models/item_genre_model.dart';
import 'package:brix_test/data/models/movie_model.dart';
import 'package:brix_test/presentation/controllers/movies_controller.dart';
import 'package:brix_test/presentation/widgets/snackbars.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:shared_preferences/shared_preferences.dart';
part 'detail_movie_controller.g.dart';

class DetailMovieController = _DetailMovieController
    with _$DetailMovieController;

abstract class _DetailMovieController with Store {
  final MoviesController controller = getIt<MoviesController>();

  @observable
  String? titleMovie = '';

  @observable
  String? directorMovie = '';

  @observable
  String? summaryMovie = '';

  @observable
  ObservableList<ItemGenreModel> listGenresMovie = ObservableList.of([]);

  @action
  void setTitle(String value) {
    titleMovie = value;
  }

  @action
  void setDirector(String value) {
    directorMovie = value;
  }

  @action
  void setSummary(String value) {
    summaryMovie = value;
  }

  @action
  void selectGenres(int index) {
    ItemGenreModel data = listGenresMovie[index];
    data.isSelected = !data.isSelected;
    listGenresMovie = ObservableList.of(listGenresMovie);
  }

  @action
  void fetchGenres({List<ItemGenreModel>? listGenres}) {
    if (listGenres?.isEmpty ?? false || listGenres == []) {
      listGenresMovie = ObservableList.of(ItemGenreModel.chipsList);
    } else {
      listGenresMovie =
          ObservableList.of(listGenres ?? ItemGenreModel.chipsList);
    }
  }

  @action
  Future addOrUpdateNewMovie(BuildContext context, {MovieModel? movie}) async {
    List<ItemGenreModel> listMovieSelected =
        listGenresMovie.where((element) => element.isSelected).toList();
    if (titleMovie?.isEmpty ?? false) {
      showAlertSnackBar(context, 'Title can not be empty');
    } else if (directorMovie?.isEmpty ?? false) {
      showAlertSnackBar(context, 'Director can not be empty');
    } else if (summaryMovie?.isEmpty ?? false) {
      showAlertSnackBar(context, 'Summary can not be empty');
    } else if (listMovieSelected.isEmpty) {
      showAlertSnackBar(context, 'Please select genre');
    } else {
      if (movie == null) {
        controller.listFilterMovies.add(MovieModel(
            title: titleMovie,
            director: directorMovie,
            summary: summaryMovie,
            genres: listGenresMovie));
      } else {
        int index = controller.listFilterMovies.indexOf(movie);
        controller.listFilterMovies[index] = MovieModel(
            title: titleMovie,
            director: directorMovie,
            summary: summaryMovie,
            genres: listGenresMovie);
      }

      try {
        final sharedPref = await SharedPreferences.getInstance();
        sharedPref.setString(
            keyMovies, encodeToListMovie(controller.listFilterMovies));
        context.router.pushAndPopUntil(HomeRoute(), predicate: (_) => false);
        disposeData();
        showSuccessSnackBar(
            context, 'Success ${movie == null ? 'add' : 'update'} new movies');
      } catch (e) {
        print(e);
      }
    }
  }

  @action
  deleteMovie(BuildContext context, MovieModel movie) async {
    final sharedPref = await SharedPreferences.getInstance();
    int index = controller.listFilterMovies.indexOf(movie);
    controller.listFilterMovies.removeAt(index);

    sharedPref.setString(
        keyMovies, encodeToListMovie(controller.listFilterMovies));
    disposeData();
    showAlertSnackBar(context, 'Success remove movies');
    context.router.pushAndPopUntil(HomeRoute(), predicate: (_) => false);
  }

  @action
  disposeData() {
    titleMovie = '';
    directorMovie = '';
    summaryMovie = '';
    for (var element in listGenresMovie) {
      element.isSelected = false;
    }
  }
}
