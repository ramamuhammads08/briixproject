import 'dart:convert';
import 'package:brix_test/core/utils/util.dart';
import 'package:brix_test/data/models/movie_model.dart';
import 'package:mobx/mobx.dart';
import 'package:shared_preferences/shared_preferences.dart';
part 'movies_controller.g.dart';

class MoviesController = _MoviesController with _$MoviesController;

abstract class _MoviesController with Store {
  @observable
  ObservableList<MovieModel> listMovies = ObservableList.of([]);

  @observable
  ObservableList<MovieModel> listFilterMovies = ObservableList.of([]);

  @action
  Future fetchMovies() async {
    try {
      final sharedPref = await SharedPreferences.getInstance();
      if (sharedPref.getString(keyMovies) != null) {
        Iterable data = jsonDecode(sharedPref.getString(keyMovies) ?? '');
        final movies = data.map((model) => MovieModel.fromMap(model)).toList();
        listMovies = ObservableList.of(movies);
        listFilterMovies = ObservableList.of(movies);
      } else {
        listMovies = ObservableList.of([]);
      }
    } catch (e) {
      print(e);
    }
  }

  @action
  void searchMovies(String keyword) {
    if (keyword.isEmpty) {
      listFilterMovies = listMovies;
    } else {
      final List<MovieModel> searchList = listMovies
          .where((element) =>
              element.title!.toLowerCase().contains(keyword.toLowerCase()))
          .toList();
      listFilterMovies = ObservableList.of(searchList);
    }
  }
}
