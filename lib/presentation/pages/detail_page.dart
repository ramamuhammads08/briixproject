import 'package:auto_route/auto_route.dart';
import 'package:brix_test/core/di/locator.dart';
import 'package:brix_test/data/models/movie_model.dart';
import 'package:brix_test/presentation/controllers/detail_movie_controller.dart';
import 'package:brix_test/presentation/widgets/text_form_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

@RoutePage()
class DetailPage extends StatelessWidget {
  final DetailMovieController controller = getIt<DetailMovieController>();
  final MovieModel? movie;
  
  DetailPage({this.movie, super.key}) {
    controller.setTitle(movie?.title ?? '');
    controller.setDirector(movie?.director ?? '');
    controller.setSummary(movie?.summary ?? '');
    controller.fetchGenres(listGenres: movie?.genres);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: movie == null
          ? () async {
              controller.disposeData();
              return true;
            }
          : () async {
              return true;
            },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black87,
          actions: [
            movie == null
                ? const SizedBox()
                : IconButton(
                    onPressed: () {
                      controller.deleteMovie(context, movie!);
                    },
                    icon: const Icon(Icons.remove_circle,
                        color: Colors.redAccent)),
            IconButton(
                onPressed: () {
                  controller.addOrUpdateNewMovie(context, movie: movie);
                },
                icon: const Icon(Icons.save, color: Colors.white))
          ],
        ),
        body: SafeArea(
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
            children: [
              CustomTextFormField(
                  controller:
                      TextEditingController(text: controller.titleMovie),
                  label: 'Title',
                  onChanged: (val) {
                    controller.setTitle(val);
                  }),
              const SizedBox(height: 16),
              CustomTextFormField(
                  controller:
                      TextEditingController(text: movie?.director ?? ''),
                  label: 'Director',
                  onChanged: (val) {
                    controller.setDirector(val);
                  }),
              const SizedBox(height: 16),
              CustomTextFormField(
                  controller: TextEditingController(text: movie?.summary ?? ''),
                  label: 'Summary',
                  maxLines: 10,
                  onChanged: (val) {
                    controller.setSummary(val);
                  }),
              const SizedBox(height: 16),
              Observer(
                builder: (_) => Wrap(
                    spacing: 8,
                    direction: Axis.horizontal,
                    children: controller.listGenresMovie
                        .map((movie) => Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 5),
                              child: FilterChip(
                                label: Text(movie.label),
                                labelStyle: const TextStyle(
                                    color: Colors.white, fontSize: 16),
                                backgroundColor: Colors.black54,
                                selectedColor: Colors.black,
                                checkmarkColor: Colors.white,
                                selected: movie.isSelected,
                                onSelected: (_) {
                                  controller.selectGenres(controller
                                      .listGenresMovie
                                      .indexOf(movie));
                                },
                              ),
                            ))
                        .toList()),
              )
            ],
          ),
        ),
      ),
    );
  }
}
