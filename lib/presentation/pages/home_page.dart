import 'package:auto_route/auto_route.dart';
import 'package:brix_test/app/routers/app_router.gr.dart';
import 'package:brix_test/core/di/locator.dart';
import 'package:brix_test/presentation/controllers/movies_controller.dart';
import 'package:brix_test/presentation/widgets/items.dart';
import 'package:brix_test/presentation/widgets/text_form_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

@RoutePage()
class HomePage extends StatelessWidget {
  final MoviesController controller = getIt<MoviesController>();

  HomePage({super.key}) {
    controller.fetchMovies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 4,
        backgroundColor: Colors.black87,
        title: const Text('Movies Collection'),
      ),
      body: Stack(
        children: [
          controller.listFilterMovies.isEmpty
              ? const Center(
                  child: Text('List movie is empty...',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Colors.black87,
                          fontSize: 15)))
              : Column(children: [
                  Padding(
                      padding: const EdgeInsets.all(16),
                      child:
                          SearchTextField(onChanged: controller.searchMovies)),
                  Observer(
                      builder: (_) => Expanded(
                            child: ListView.separated(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 16),
                                shrinkWrap: true,
                                itemCount: controller.listFilterMovies.length,
                                itemBuilder: (context, index) => MovieItem(
                                    onTap: (movie) {
                                      context.router
                                          .push(DetailRoute(movie: movie));
                                    },
                                    movie: controller.listFilterMovies[index]),
                                separatorBuilder: (context, index) =>
                                    const SizedBox(height: 16)),
                          )),
                ]),
          Positioned(
              bottom: 24,
              right: 16,
              child: FloatingActionButton(
                  backgroundColor: Colors.black87,
                  child: const Icon(Icons.add),
                  onPressed: () {
                    context.router.push(DetailRoute());
                  }))
        ],
      ),
    );
  }
}
