import 'package:brix_test/app/routers/app_router.dart';
import 'package:brix_test/core/di/locator.dart';
import 'package:flutter/material.dart';

void main() async {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  MainApp({super.key}) {
    setupDI();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      routerConfig: getIt.get<AppRouter>().config(),
    );
  }
}
