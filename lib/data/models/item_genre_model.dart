class ItemGenreModel {
  String label;
  bool isSelected;

  ItemGenreModel(this.label, this.isSelected);

  factory ItemGenreModel.fromMap(Map<String, dynamic> map) =>
      ItemGenreModel(map['label'], map['isSelected']);

  Map<String, dynamic> toJson() => {'label': label, 'isSelected': isSelected};

  static final List<ItemGenreModel> chipsList = [
    ItemGenreModel("Action", false),
    ItemGenreModel("Animation", false),
    ItemGenreModel("Drama", false),
    ItemGenreModel("Sci-Fi", false),
    ItemGenreModel('Horror', false)
  ];
}
