import 'package:brix_test/core/utils/extensions.dart';
import 'package:brix_test/data/models/item_genre_model.dart';

class MovieModel {
  String id = 'abcdefghijklmnopqrstuvwxyz0123456789'.generateId();
  String? title, director, summary;
  List<ItemGenreModel>? genres;

  MovieModel({this.title, this.director, this.summary, this.genres});

  factory MovieModel.fromMap(Map<String, dynamic> map) => MovieModel(
      title: map['title'],
      director: map['director'],
      summary: map['summary'],
      genres: List<ItemGenreModel>.from(
          map['genres'].map((e) => ItemGenreModel.fromMap(e))));

  Map<String, dynamic> toJson() => {
        'title': title,
        'director': director,
        'summary': summary,
        'genres': List<dynamic>.from(genres!.map((e) => e.toJson()))
      };

  MovieModel copyWith(
          {String? title,
          String? director,
          String? summary,
          List<ItemGenreModel>? genres}) =>
      MovieModel(
          title: title ?? this.title,
          director: director ?? this.director,
          summary: summary ?? this.summary,
          genres: genres ?? this.genres);
}
