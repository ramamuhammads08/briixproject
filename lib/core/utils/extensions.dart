import 'dart:convert';
import 'dart:math';

import 'package:brix_test/data/models/movie_model.dart';

extension StringExtension on String {
  String generateId() {
    var random = Random();
    String id = '';
    for (var i = 0; i < 10; i++) {
      id += this[random.nextInt(length)];
    }
    return id;
  }
}

String encodeToListMovie(List<MovieModel> listMovie) =>
    jsonEncode(listMovie.map((e) => e.toJson()).toList());
