import 'package:brix_test/app/routers/app_router.dart';
import 'package:brix_test/presentation/controllers/detail_movie_controller.dart';
import 'package:brix_test/presentation/controllers/movies_controller.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void setupDI() {
  getIt.registerSingleton<AppRouter>(AppRouter());
  getIt.registerLazySingleton<MoviesController>(() => MoviesController());
  getIt.registerLazySingleton<DetailMovieController>(() => DetailMovieController());
}
